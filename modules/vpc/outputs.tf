output "vpc_id" {
  value       = aws_vpc.main.id
  description = "Id of the VPC created in this module."
}
output "cidr_block" {
  value       = var.cidr_block
  description = "CIDR block for the VPC."
}
output "azs_to_nat_ids" {
  value       = zipmap(local.nat_azs, aws_nat_gateway.main[*].id)
  description = "Mapping of availability zone to NAT gateway id."
}
output "azs_to_subnet_ids" {
  value       = zipmap(local.subnet_azs, aws_subnet.public[*].id)
  description = "Mapping of availability zone to public subnet id."
}
output "public_subnet_acl_ids" {
  value       = aws_network_acl.public.id
  description = "Public subnet acl id."
}
